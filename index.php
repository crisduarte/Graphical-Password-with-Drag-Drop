<!DOCTYPE html>
<html>
<head>
	<title>Proyecto Final de Grado - Cristhian Duarte Bogado</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<script type="text/javascript" src="jquery-ui/external/jquery/jquery.js"></script>
	<script type="text/javascript" src="jquery-ui/jquery-ui.js"></script>
	<link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.css">

	<link rel="shortcut icon" type="image/x-icon" href="static/logo.ico" />
	
	<script type="text/javascript" src="js/DragDropTouch.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>
<body>
	<div id="all">
	<?php require('header.html'); 
			require('login.php');
	?>
	
	<br>
	<?php require('footer.html'); ?>
	</div>
</body>
</html>