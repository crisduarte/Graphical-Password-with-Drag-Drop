<?php
	//código de encriptación y desencriptación simétrica
	$key = hex2bin('5ae1b8a17bad4da4fdac796f64c16ecd');
	$iv = hex2bin('34857d973953e44afb49ea9d61104d8c');
	function sen($plano)
	{
		$data = openssl_encrypt($plano, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);		
		return $data;
	}	//encriptación

	require('kgen.php');
	function aen($p)
	{
		$var = $p;
		// Get the public Key of the recipient
		$publicKey = openssl_pkey_get_public(file_get_contents('key/public.key'));
		$a_key = openssl_pkey_get_details($publicKey);
		//Encryp!!!
		openssl_public_encrypt($var, $encrypted, $publicKey);
		openssl_free_key($publicKey);
		return $encrypted;
	}

	function encr($plano)
	{
		$var = $plano;
		$a = sen($var);
		$b = aen($a);
		$aux = urlencode($b);
		return $aux;
	}
?>