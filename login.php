<div class="panel panel-default">
	<h2 id="titulo">Ingresar! Por favor Ingresa tu usuario y Contraseña</h2>
	<div class="panel-body">
		<div class="form-group">
			<div class="input-group">
			  <div class="input-group-addon">
			      <span class="glyphicon glyphicon-user">
			      </span>
			  </div>
			  <input id="u" type="text" class="form-control" placeholder="Nombre de Usuario o Correo Electrónico" onfocus="inicioU()" onblur="finU()">
			</div> 
			<span class="help-block" id="error">    
			</span>

			<div class="input-group">
			  <div class="input-group-addon">
			      <span class="glyphicon glyphicon-log-in">
			      </span>
			  </div>
			  <input id="c" type="text" class="form-control" placeholder="Contraseña" disabled="true">
			</div> 
			<span class="help-block" id="error">    
			</span>
		</div>

		<div id="mainModule">
			<div id="box" ondragenter="cDragEnter(event)" ondragleave="cDragLeave(event)" ondragover="cDragOver(event)" ondrop="cDrop(event)" drag>
				
			</div>
			<br>
			<div id="alado"><!-- 
				 --><button id="atras" onclick="atr()" type="button" class="btn btn-default">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</button><!-- 
				
				 --><div id="keyboard">
					<?php 
					require('1A.php'); ?>
				</div><!-- 
				 --><button id="siguiente" onclick="sig()" type="button" class="btn btn-default">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</button><!-- 
			 --></div>
			<div class="btnsbox">
				<div class="btn-toolbar" role="toolbar">
				 
				  <button onclick="corregir()" type="button" class="btn btn-danger">
				    <span class="glyphicon glyphicon-trash"></span> Corregir Contraseña
				  </button>
				 
				  <button onclick="ayuda()" type="button" class="btn btn-info">
				    <span class="glyphicon glyphicon-info-sign"></span> Ayuda
				  </button>
				</div>
			</div>
			
		</div>
		<div id="dialog-message" title="Como ingresar la contraseña">
		  	<p>
		    Para poder crear una contraseña,<b> debes arrastrar las imagenes que ves desde su lugar hasta el cuadro con borde de puntos que está arriba de las imagenes y luego soltarla</b>, si quieres elegir otras imagenes <b>haz click en los botones amarillos de los lados</b>.
		  </p>
		</div>
		<div class="btnbox">
			<button type="button" class="btn btn-primary btn-lg btn-block" onclick="login()">Enviar</button>
			<br>
			<button id="btnDesertar" type="button" class="btn btn-primary btn-lg btn-danger" onclick="desertar()" style="display: none;">Olvidé mi contraseña</button>
		</div>
	</div>
</div>
